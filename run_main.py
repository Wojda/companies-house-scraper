import api
import win32com.client
import cProfile


def main():
    #Connet to the currently open excel file
    xlApp = win32com.client.GetActiveObject('Excel.Application')

    #Name of the excel file
    xlWrkbook = xlApp.Workbooks('macro.xlsm')

    #Name of worksheet
    xlWrksht = xlWrkbook.Worksheets('Companies house')

    #Get the value of register number from excel file
    register_number = xlWrksht.Cells(1,2).Value


    #Use strip method if someone coppy pasted the value whith space
    register_number = register_number.strip()

    #Get company profile data from api
    company_info = api.company_profile(register_number)

    if company_info == Exception:
        xlWrksht.Cells(2, 2).Value = 'Check whether the GB number is correct.'
    if company_info == ConnectionError:
        xlWrksht.Cells(2, 2).Value = 'Check you internet connection'
    else:
        xlWrksht.Cells(2, 2).Value = company_info.company_name
        xlWrksht.Cells(3, 2).Value = company_info.registered_office
        xlWrksht.Cells(4, 2).Value = company_info.reference_date
        xlWrksht.Cells(5, 2).Value = company_info.incorporation_date


    #Part for termination/appointment
    try:
        start_cell = 6
        for appointment_key, appointment_value in api.company_officers(register_number).items():
            xlWrksht.Cells(start_cell, 2).value = appointment_key
            xlWrksht.Cells(start_cell, 3).value = appointment_value
            start_cell += 1
    except AttributeError:
        if company_info == Exception:
            pass
        if company_info == ConnectionError:
            pass
        else:
            xlWrksht.Cells(6, 2).value = 'There was no appointments/resignations'

if __name__ == '__main__':
    main()

