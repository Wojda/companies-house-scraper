import requests
from collections import namedtuple
import calendar

#TODO: It is possible that api kay needs to be updated (soulution: Api key in separate file, eg. text file)
API_KEY = 'F7WK42kntobmjKeT9Yjlb5Ab3uiE19twfiGicpZD'


#gets company_profile data from api
def company_profile(registered_number):
    try:
        results = requests.get(f'https://api.companieshouse.gov.uk/company/{registered_number}', auth=(API_KEY,''))
        json_results = results.json()
        company_profile = []
        company_profile.append(json_results['company_name'])

        office_list = []
        for location in json_results["registered_office_address"].values():
            office_list.append(location)
        office = ", ".join(office_list)
        company_profile.append(office)
        ref_date =json_results['accounts']['accounting_reference_date']

        month = calendar.month_name[int(ref_date["month"])]
        company_profile.append(f'Day: {ref_date["day"]} Month: {ref_date["month"]} ({month})')
        company_profile.append(json_results['date_of_creation'])

        #Namedtuple - for better readabilty
        Company_Profile = namedtuple('Company_Profile',  ['company_name', 'registered_office', 'reference_date', 'incorporation_date'])
        company_profile = Company_Profile._make(company_profile)

    except TypeError:
        return Exception
    except ConnectionError:
        return Exception
    except KeyError:
        return Exception
    except requests.exceptions.ConnectionError:
        return ConnectionError
    else:
        return company_profile

#company officers data
def company_officers(registered_number):
    try:
        results = requests.get(f'https://api.companieshouse.gov.uk/company/{registered_number}/filing-history?category=officers', auth=(API_KEY,''))
        json_results = results.json()
        company_officers = {}

        for type in json_results['items']:
            officer_name = type["description_values"]["officer_name"]
            if type['description'] == 'appoint-person-director-company-with-name-date':
                company_officers[f'Appointment of Director {officer_name}'] = type["description_values"]["appointment_date"]
            if type['description'] == 'termination-director-company-with-name-termination-date':
                company_officers[f'Termination of Director {officer_name}'] = type["description_values"]["termination_date"]
            if type['description'] == 'termination-director-company-with-name':
                company_officers[f'Termination of Director {officer_name}'] = type["date"]
            if type['description'] == 'appoint-person-director-company-with-name':
                company_officers[f'Appointment of Director {officer_name}'] = type["date"]
            if type['description'] == 'termination-secretary-company-with-name':
                company_officers[f'Termination of Secretary {officer_name}'] = type["date"]
            if type['description'] == 'termination-secretary-company-with-name':
                company_officers[f'Termination of Secretary {officer_name}'] = type["date"]
            if type['description'] == 'termination-secretary-company-with-name-termination-date':
                company_officers[f'Termination of Secretary {officer_name}'] = type["date"]
            if type['description'] == 'appoint-person-secretary-company-with-name-date':
                company_officers[f'Appointment of Secretary {officer_name}'] = type["description_values"]["appointment_date"]
    except TypeError:
        return Exception
    except ConnectionError:
        return Exception
    except KeyError:
        return Exception
    except requests.exceptions.ConnectionError:
        return ConnectionError
    else:
        return company_officers
